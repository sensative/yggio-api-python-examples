"""
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""


# This script set up a new node and post data to it

import requests
import os
import yaml

#Set root to run locally
if os.getcwd()[0:1]== '/': #You are on linux, eg. at work
    working_path = '/home/sm/yggio-api-python-examples'
elif os.getcwd()[0:1]== 'C': #You are on Window, eg. at home
    working_path = 'C:/Users/sara/Documents/yggio-api-python-examples'
os.chdir(working_path)

#Own modules
import yggio_API



###########################################################################################
# Authorize
###########################################################################################

#Load config and its parameters
with open("config.yaml", "r") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)
server = config["yggio_account"]["server"]
username= config["yggio_account"]["username"]
password = config["yggio_account"]["password"]


#Authorize
my_headers = yggio_API.authorize(server, username, password)

###########################################################################################
# Create node
###########################################################################################
# You can check in the "UI: Devices" that a new node with the name my_new_node is created
# You can also delete it from the UI by "Devices: my_new_node: Delete Device"

response = requests.post(
    server + '/iotnodes/',
    data = {'name': 'my_new_node'},
    headers=my_headers
)
jsonResponse = response.json()
new_nodeId = jsonResponse['_id']
print("You might want to note down the id of your new node: ", new_nodeId )


 ###########################################################################################     
# Send data to node
###########################################################################################

response = requests.put(
    server + '/iotnodes/'+new_nodeId,
    data = {
      "anomaly": 0
    },
    headers=my_headers
)
jsonResponse = response.json()