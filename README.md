# Yggio Api Python Examples

This repo contains "How-to"- like scripts of how to interact with Yggio through Python. The "How-to"- guides target audience are persons doing machine learning.

## Overview

| Key concepts used        | Script name           | Used for  |
| ------------- |-------------| -----|
| API      | get_data_from_API | Get data out of Yggio for local training. |
| API      | set_up_node_and_post_data      |   Creates a new node on Yggio and post data to it. Useful for posting model output back to Yggio for stearing a device via Yggio Rule Engine, f.ex. turn on a light bulb. |
| channel | set_up_channel      | Creates mqtt- channels which allows you to listen to incomming data in real time. Useful to allow for triggering a modell when a new datapoint arrives (instead of fetching from the API at regular intervals).  |
| channel | get_data_from_channel      | Run a loop that listen to a channel and prints out incomming data in real time. |
|  | yggio_API      |    Common functions across all scripts. |
