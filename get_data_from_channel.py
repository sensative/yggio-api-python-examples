"""
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

# This scripts runs a loop that listen to a channel. It is used to get data from a sensor in real time.

import os
import yaml
import json
import requests
import time
import paho.mqtt.client as mqtt
import numpy as np


#Set root to run locally
if os.getcwd()[0:1]== '/': #You are on linux, eg. at work
    working_path = '/home/sm/yggio-api-python-examples'
elif os.getcwd()[0:1]== 'C': #You are on Window, eg. at home
    working_path = 'C:/Users/sara/Documents/yggio-api-python-examples'
os.chdir(working_path)

#Own modules
import yggio_API

def update_inputs(inputs, value):
    inputs = np.append(inputs, [value])
    inputs = np.delete(inputs, 0)
    return inputs

def on_connect(client, userdata, flags, rc):
    # This will be called once the client connects
    if rc == 0:
        pass
        #print("Connected to MQTT Broker!")
    else:
        print("Failed to connect, return code %d\n", rc)
    # Subscribe here!
    client.subscribe(topic)
    
def on_message(client, userdata, msg):
    #print(f"Message received [{msg.topic}]: {msg.payload}")
    #messages.append(msg.payload)
    values = msg.payload
    values = json.loads(values.decode('utf-8'))
    values = values['diff']['value']
    if measurement in values:
        time = values['encodedData']['timestamp']
        print('Timestamp', time)
        value = values[measurement]
        inputs = userdata
        inputs = update_inputs(inputs, value)
        print("New inputs: ", inputs)
        client.user_data_set(inputs)

###########################################################################################
# Parameters
###########################################################################################

#Load config and its parameters
with open("config.yaml", "r") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)
server = config["yggio_account"]["server"]
username= config["yggio_account"]["username"]
password = config["yggio_account"]["password"]
username_base = config["basic_credential_set"]["username_base"]
password_base = config["basic_credential_set"]["password_base"]

#Data and channel param
nodeId = '62025fc93619740009525959'
measurement = 'irProximity'
basicCredentialsSetId = '631ee9da8ae8c6000ab836bd'

#Derrive rest of param
nodeId4last = nodeId[len(nodeId)-4:len(nodeId)]
channel_username = username_base + nodeId4last
channel_password = password_base + nodeId4last
topic = 'yggio/output/v2/'+basicCredentialsSetId+'/iotnode/'+nodeId
mqqt_path = 'mqtt.'+ server[server.index("/")+2:len(server)].replace('/api', '')


###########################################################################################
# Init
###########################################################################################
#Authorize
my_headers = yggio_API.authorize(server, username, password)
my_session = requests.Session()
my_session.headers.update(my_headers)

#Decide data to load
starttime = 0
endtime = int(round(time.time() * 1000))

#Load last 3 timepoints from API
df = yggio_API.collectOnePeriodOneNode(nodeId, measurement, starttime, endtime, server, username, password, my_session)
inputs=np.flip(df[0:3].value.to_numpy()) #Get the last 3 timepoints, last one at the end of array
 
###########################################################################################
# Loop that reacts on each message from channel
###########################################################################################

client = mqtt.Client("mqqt-test", userdata=inputs) # client ID "mqtt-test"
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(channel_username, channel_password)
client.user_data_set(inputs) 
client.connect(mqqt_path, 1883)
client.loop_forever()  # Start networking daemon