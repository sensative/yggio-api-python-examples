"""
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import requests
import os
import json
import yaml

#Set root to run locally
if os.getcwd()[0:1]== '/': #You are on linux, eg. at work
    working_path = '/home/sm/yggio-api-python-examples'
elif os.getcwd()[0:1]== 'C': #You are on Window, eg. at home
    working_path = 'C:/Users/sara/Documents/yggio-api-python-examples'
os.chdir(working_path)

#Own modules
import yggio_API


###########################################################################################
# Authorize
###########################################################################################

#Load config and its parameters
with open("config.yaml", "r") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)
server = config["yggio_account"]["server"]
username= config["yggio_account"]["username"]
password = config["yggio_account"]["password"]

#Authorize
my_headers = yggio_API.authorize(server, username, password)
my_session = requests.Session()
my_session.headers.update(my_headers)


###########################################################################################
# Create mqtt- channels 
###########################################################################################
# Specify which nodes you want channels on (You need one channel for each node you want to listen to)
# You can check in the "UI: Devices:Some specific device: Data: All" for the _id 

series = [{"_id": "62025fc93619740009525959"},
        {"_id": "628de399023724000a04b495"}
        ]

# To create a channel you first need a "basic credential set", so you will need one per node/channel.
# I find it useful to have one base username and password for credential sets and then vary the node
# specific username and password by appending the last four nodeId characters, which is reflected in
# the loop below
username_base = config["basic_credential_set"]["username_base"]
password_base = config["basic_credential_set"]["password_base"]


# Create channels
for idx in range(len(series)):
    
    nodeId = series[idx]['_id']
    four_last_char_in_id = nodeId[len(nodeId)-4:len(nodeId)]
    
    #Create basicCredentialSet
    response =my_session.post(server +'/basic-credentials-sets', json={"username": username_base + four_last_char_in_id, "password": password_base + four_last_char_in_id})
    if response.status_code == 201:
        response_data = response.json()
        basicCredentialsSetId = response_data['_id'] 
    else:
        print("Cannot set basicCredentialSet. Status code", response.status_code)
        
    #Create channel
    response =my_session.post(server +'/channels', json={
      "name": 'anomaly_detection',
      "iotnode": nodeId,
      "mqtt": {
        "type": 'basicCredentialsSet',
        "recipient": basicCredentialsSetId
      }
    })
    if response.status_code == 201:
        response_data = response.json() 
    else:
        print("Cannot create channel. Status code", response.status_code)
     
    #Append to series for downstream use
    series[idx]['basicCredentialsSetId'] = basicCredentialsSetId
    

print("You most likely would like to save the basicCredentialsSetId:s for downstream use:", series)


# For usage when things go wrong with your basicCredentialSets:
    
# # # Check if/which basicCredentialSets exists
# response =my_session.get(server +'/basic-credentials-sets')
# if response.status_code == 200:
#     response_data = response.json()
#     print("basicCredentialSets:", response_data)
    
# # Delete
# response =my_session.delete(server +'/basic-credentials-sets/'+'631ee9db8ae8c6000ab836c4')
# if response.status_code == 204:
#     print("basicCredentialSet deleted")